#!/bin/bash

set -x

if [ $(sudo systemctl is-active systemd-binfmt.service | tr -d '\n') != "active" ]
then
	printf "Service is not active, enabling...\n"
	exit
	sudo apt install qemu qemu-system-misc qemu-user-static qemu-user binfmt-support

	sudo mkdir -p /lib/binfmt.d

	sudo sh -c 'echo :qemu-aarch64:M::\\x7f\\x45\\x4c\\x46\\x02\\x01\\x01\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x02\\x00\\xb7\\x00:\\xff\\xff\\xff\\xff\\xff\\xff\\xff\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\xfe\\xff\\xff\\xff:/usr/bin/qemu-aarch64-static:F > /lib/binfmt.d/qemu-aarch64-static.conf'

	sudo systemctl restart systemd-binfmt.service
else
	printf "Service is already active, you should be able to chroot into aarch64 rootfs...\n"
fi
