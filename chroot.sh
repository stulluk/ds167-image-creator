#!/bin/bash

ROOTDIR="rootfs"
EMULATOR="qemu-aarch64-static"

if [ $(id -u) = "0" ]; then
    echo "You are root"
else
    echo "You are NOT root, exiting"
    exit 5
fi

function cleanup(){

umount ${ROOTDIR}/proc
umount ${ROOTDIR}/sys
umount ${ROOTDIR}/dev/pts
umount ${ROOTDIR}/dev
umount ${ROOTDIR}/tmp

}

trap cleanup EXIT

#


# Copy qemu-user-static to rootfs if necessary
if [ -f "${ROOTDIR}/usr/bin/${EMULATOR}" ]
then
	printf "Emulator is already copied to rootfs...\n"
else
	printf "Copying emulator to rootfs...\n"
	sudo cp $(which ${EMULATOR}) ${ROOTDIR}/usr/bin/
fi


#Mount required paths

mount -o bind /proc ${ROOTDIR}/proc
mount -o bind /tmp ${ROOTDIR}/tmp
mount -o bind /sys ${ROOTDIR}/sys
mount -o bind /dev ${ROOTDIR}/dev
mount -o bind /dev/pts ${ROOTDIR}/dev/pts



if [ ! -f /proc/sys/fs/binfmt_misc/aarch64 ]
then
	printf "Not registered, registering...\n"
	echo ':aarch64:M::\x7fELF\x02\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00\xb7:\xff\xff\xff\xff\xff\xff\xff\xfc\xff\xff\xff\xff\xff\xff\xff\xff\xfe\xff\xff:/usr/bin/qemu-aarch64-static:' > /proc/sys/fs/binfmt_misc/register
	#echo ':arm:M::\x7fELF\x01\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00\x28\x00:\xff\xff\xff\xff\xff\xff\xff\x00\xff\xff\xff\xff\xff\xff\xff\xff\xfe\xff\xff\xff:/usr/bin/qemu-aarch64-static:' > /proc/sys/fs/binfmt_misc/register
	#echo ':aarch64:M::\x7fELF\x02\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00\xb7\x00:\xff\xff\xff\xff\xff\xff\xff\x00\xff\xff\xff\xff\xff\xff\xff\xff\xfe\xff\xff\xff:/usr/local/bin/qemu-aarch64:' > /proc/sys/fs/binfmt_misc/register
else
	printf "Already registered..\n"
fi

chroot ${ROOTDIR} qemu-aarch64-static /bin/bash

