#!/bin/bash

set -euo pipefail

printf "Starting SDcard image creator...\n"

UBOOTFILE="/D/DS167/boot/u-boot/u-boot-sunxi-with-spl.bin"
DTBFILE="/D/DS167/boot/u-boot/arch/arm/dts/sun7i-a20-interra-ds167-emmc.dtb"
# DTBFILE="/D/DS167/kernel/linux-sunxi/arch/arm/boot/dts/sun7i-a20-interra-ds167-emmc.dtb"
# DTBFILE="/D/DS167/kernel/linux-sunxi/arch/arm/boot/dts/sun7i-a20-interra-ds167-emmc.dtb"
#KERNELFILE="/D/DS167/kernel/linux-sunxi/arch/arm/boot/zImage"
KERNELFILE="/D/DS167/kernel/linux-sunxi/arch/arm/boot/zImage"
BOOTSCRFILE="/D/DS167/boot/boot.cmd/boot.scr"
#ROOTFSDIR="/D/DS167/rootfs/focal-base-armhf/"
ROOTFS_TARBALL="/D/DS167/rootfs/armhf-rootfs-ubuntu-focal.tar"

DISKIMG="ds167_sd.img"
DISKSIZE="2048" #MegaBytes
BLOCKSIZE="16" #MegaBytes
PARTEDFILE="ds167_sd.parted"

BOOT_MOUNT_DIR="/tmp/SDCARD/BOOT"
ROOTFS_MOUNT_DIR="/tmp/SDCARD/ROOTFS"

# Remove stale files that are created previously

rm -f ${DISKIMG}
rm -f ${PARTEDFILE}

# Unmount MOUNT DIRS if they were already mounted

if [ $(grep -c ${BOOT_MOUNT_DIR} /proc/mounts) -gt 0 ]
then
	umount ${BOOT_MOUNT_DIR}
fi

if [ $(grep -c ${ROOTFS_MOUNT_DIR} /proc/mounts) -gt 0 ]
then
	umount ${ROOTFS_MOUNT_DIR}
fi

# Cleanup handler

function cleanup(){

	losetup -D

#umount ${ROOTDIR}/proc
#umount ${ROOTDIR}/sys
#umount ${ROOTDIR}/dev/pts
#umount ${ROOTDIR}/dev
#umount ${ROOTDIR}/tmp

printf "exiting cleanup()...\n"

}

trap cleanup EXIT

# Check rootness

if [ $(id -u) = "0" ]; then
    echo "You are root"
else
    echo "You are NOT root, exiting"
    exit 5
fi

# Create empty diskimage

printf "\nCreating empty disk file of size = %d MegaBytes \n" "${DISKSIZE}"
dd if=/dev/zero of=${DISKIMG} bs=${BLOCKSIZE}M count=$((${DISKSIZE}/${BLOCKSIZE}))

# Create two partitions, BOOT=128Mbyte FAT32 and ROOTFS=Remaining Mbytes with EXT4
# First create a parted file and apply this file to disk image.

printf "\nCreating partedfile...\n"

cat > ${PARTEDFILE} <<EOF
mktable msdos
mkpart primary fat32 1M 128M
mkpart primary ext4 128M -1M
quit
EOF

printf "\nApplying partition layout to disk...\n"
parted ${DISKIMG} < ${PARTEDFILE}

# Find loopback device

printf "\nFinding available loopback device for our disk...\n"

LOOPNAME=$(losetup --partscan --show --find ${DISKIMG})

BOOTLOOP="${LOOPNAME}p1"
ROOTFSLOOP="${LOOPNAME}p2"

printf "\nFormatting boot partition..\n"

mkfs.vfat -F 32 "${BOOTLOOP}"

printf "\nFormatting rootfs partition..\n"
mkfs.ext4 "${ROOTFSLOOP}"

# losetup -d ${LOOPNAME}

# Loop mount boot partition

printf "\nMount boot partition..\n"

mkdir -p ${BOOT_MOUNT_DIR}
mount -o loop ${BOOTLOOP} ${BOOT_MOUNT_DIR}

BOOT_UUID=$(lsblk -n --output UUID ${BOOTLOOP} | tr -d '\n')

# Copy DTB to boot
printf "\nCopy DTB to boot..\n"

cp ${DTBFILE} ${BOOT_MOUNT_DIR}

# Copy zImage to boot
printf "\nCopy zImage to boot..\n"

cp ${KERNELFILE} ${BOOT_MOUNT_DIR}

# Copy boot.scr to boot
printf "\nCopy boot.scr to boot..\n"

cp ${BOOTSCRFILE} ${BOOT_MOUNT_DIR}

# Umount boot
printf "\nUmount boot..\n"

umount ${BOOT_MOUNT_DIR}

# Loop mount rootfs
printf "\nMount rootfs partition..\n"

mkdir -p ${ROOTFS_MOUNT_DIR}
mount -o loop ${ROOTFSLOOP} ${ROOTFS_MOUNT_DIR}

# Decompress Rootfs tarball into rootfs mount dir
printf "\nDecompressing Rootfs tarball into rootfs mount dir....\n"

tar xfp ${ROOTFS_TARBALL} -C ${ROOTFS_MOUNT_DIR}

#mv ${ROOTFS_MOUNT_DIR}/binary/* ${ROOTFS_MOUNT_DIR}/
sync
sudo chown root:root ${ROOTFS_MOUNT_DIR}
sudo chmod 755 ${ROOTFS_MOUNT_DIR}

# Copy rootfs to rootfs partition

#printf "\nCopy rootfs to rootfs partition...\n"

#rsync -a --info=progress2 ${ROOTFSDIR}/* ${ROOTFS_MOUNT_DIR}/

# Find UUID of ROOTFS

ROOTFS_UUID=$(lsblk -n --output UUID ${ROOTFSLOOP} | tr -d '\n')

printf "\nWriting FSTAB ...\n"

echo "UUID=${ROOTFS_UUID} / auto defaults 0 1" > ${ROOTFS_MOUNT_DIR}/etc/fstab
echo "UUID=${BOOT_UUID} /boot auto defaults 0 2" >> ${ROOTFS_MOUNT_DIR}/etc/fstab

# Copy kernel deb package to rootfs

# Chroot to rootfs

# install kernel deb

# install some packages

# exit from chroot

# Umount rootfs
printf "\nUmount rootfs...\n"
umount "${ROOTFS_MOUNT_DIR}"

# dd u-boot to diskimage

printf "Burning u-boot to disk image...\n"
dd if=${UBOOTFILE} of=${DISKIMG} bs=1024 seek=8 conv=notrunc

# sync

sync

# Remove loop mount

#losetup -d ${LOOPNAME}

# Succesfull exit
printf "\nExiting succesfully..\n"
exit 0
